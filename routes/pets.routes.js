const express = require('express');
const Pet = require('../models/Pets');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
    const pets = await Pet.find();
    return res.json(pets);
    } catch(error) {
        next(new Error(error));
    }
});

router.get('/:id', async (req, res) => {
    try {
    const { id } = req.params;
    const pet = await Pet.findById(id);
    if(pet) {
        return res.json(pet);
    } else {
        return res.status(404).json("No pet found for this id")
    }
    } catch(error) {
        next(new Error(error));
    }
});

router.delete('/:id', async (req, res) => {
    try {
    const { id } = req.params;
    const deleted = await Pet.findByIdAndDelete(id);
    if(deleted) {
        return res.status(200).json('Pet deleted');
    }
        return res.status(200).json("Pet not found");
    } catch(error) {
        next(error);
    }
});

router.get('/species/:specie', async (req, res) => {
    try {
    const { specie } = req.params;
    const petsBySpecie = await Pet.find( {specie});
    return res.json(petsBySpecie);
    } catch(error) {
        next(new Error(error));
    }
});

router.get('/age/:age', async (req, res) => {
    try {
    const { age } = req.params;
    const petsByAge = await Pet.find( {age: {$gte: age}});
    return res.json(petsByAge);
    } catch(error) {
        next(new Error(error));
    }
});

router.post('/create', async (req, res, next) => {
    try {
        const { name, age, specie, color, breed } = req.body;

        const newPet = await new Pet({name, age, specie, color, breed});

        const createdPet = await newPet.save();

        return res.json(createdPet);
    } catch (error) {
        next(error);
    }
});

module.exports = router;