const express = require('express');
const db = require('./db.js');
const indexRoutes = require('./routes/index.routes');
const petRoutes = require('./routes/pets.routes');
const shelterRoutes = require('./routes/shelters.routes');

db.connect();
const PORT = 3000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use('/', indexRoutes);
app.use('/pets', petRoutes);
app.use('/shelters', shelterRoutes);


app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    return res.status(error.status || 500).json(error.message || 'Unexpected error');
});

app.listen(PORT, () => {
    console.log(`Server listening in http://localhost:${PORT}`)
});