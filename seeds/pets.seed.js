const mongoose = require('mongoose');
const db = require('../db');
const Pet = require('../models/Pets');

const pets = [
    {
        name: "Dana",
        age: 7,
        breed: "Labrador",
        color: "Negro",
        specie: 'dog',
    },
    {
        name: "Freya",
        age: 1,
        breed: "Comun",
        color: "Tricolor",
        specie: 'cat'
    },
    {
        name: "Mia",
        age: 0,
        breed: "Pastor Aleman",
        color: "Negro",
        specie: 'dog'
    },
    {
        name: "Rumbo",
        age: 14,
        breed: "Yorkshire",
        color: "Rubio",
        specie: "dog",
    },
    {
        name: "Lolo",
        age: 3,
        breed: "Bodeguero",
        color: "Blanco",
        specie: "dog",
    }
];
mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
    // Buscar las mascotas existentes en nuestra base de datos.
    //Si tenemos mascotas, las borramos. Si no tenemos mascotas, continuamos ejecutando.
    const allPets = await Pet.find();
    

    if(allPets.length) {
        console.log('Deleting all pets...');
        await Pet.collection.drop();
    }
    })
    .catch(error => {
        console.log('Error deleting data: ', error)
    })
    .then( async() => {
        //Añadimos a nuestra base de datos las mascotas del seed.
        await Pet.insertMany(pets);
        console.log('Successfully added pets to DB...');
    })
    .finally(() => mongoose.disconnect());